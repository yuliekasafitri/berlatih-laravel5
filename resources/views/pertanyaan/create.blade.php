@extends('adminlte.master')

@section('content')
<h2>Tambah Pertanyaan</h2>
        <form action="/pertanyaan" method="POST">
            @csrf
            <div class="form-group">
                <label for="judul">Title</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul pertanyaan">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">isi</label>
                <input type="text" class="form-control" name="isi" id="isi" placeholder="Masukkan isi pertanyaan">
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
